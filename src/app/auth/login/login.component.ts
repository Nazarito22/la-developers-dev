import { Component } from '@angular/core';
import { Router } from "@angular/router";
import {AuthService} from "../../api/auth.service";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  invalidLogin: boolean;

  constructor(
    private router: Router,
    private authService: AuthService) { }

  signIn(credentials) {
    this.authService.login(credentials)
      .subscribe(result => {
        if (result)
          this.router.navigate(['/cars']);
        else
          this.invalidLogin = true;
      });
  }
}
