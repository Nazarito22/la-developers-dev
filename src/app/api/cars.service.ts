import {Injectable} from '@angular/core';
import {Car} from "../models/car";

@Injectable()
export class CarsService {

  cars: Array<Car> = [];
  brands: Array<any> = [];
  models: Array<any> = [];

  constructor() {
    this.cars = [
      {
        id: 1,
        brand: 'Hyundai',
        model: 'Elite i20',
        year: 2003,
        imgUrl: 'https://auto.ndtvimg.com/car-images/medium/hyundai/elite-i20/hyundai-elite-i20.jpg?v=2'
      },
      {
        id: 2,
        brand: 'Renault',
        model: 'Kwid',
        year: 2007,
        imgUrl: 'https://auto.ndtvimg.com/car-images/medium/renault/kwid/renault-kwid.webp?v=2'
      },
      {
        id: 3,
        brand: 'Toyota',
        model: 'Land Cruiser Prado',
        year: 2010,
        imgUrl: 'https://auto.ndtvimg.com/car-images/medium/toyota/land-cruiser-prado/toyota-land-cruiser-prado.webp?v=23'
      }
    ];

  }

  getCar(carId) {
    for (let i = 0; i < this.cars.length; i++) {
      if (this.cars[i].id === carId) {
        return this.cars[i];
      }
    }
  }

  getAll() {
    return this.cars;
  }

  getAllBrands() {
    this.brands = [
      {
        id: 1,
        name: 'Hyundai'
      },
      {
        id: 2,
        name: 'Renault'
      },
      {
        id: 3,
        name: 'Toyota',
      }
    ];

    return this.brands;
  }

  getModels(brandId) {
    this.models = [
      {
        id: 1,
        brandId: 1,
        name: 'Elite i20'
      },
      {
        id: 2,
        brandId: 2,
        name: 'Kwid'
      },
      {
        id: 3,
        brandId: 3,
        name: 'Land Cruiser Prado',
      },
      {
        id: 4,
        brandId: 3,
        name: 'Camry',
      },
      {
        id: 5,
        brandId: 3,
        name: 'Corolla',
      },
      {
        id: 6,
        brandId: 3,
        name: 'Camry',
      },
      {
        id: 7,
        brandId: 2,
        name: 'Florida',
      },
      {
        id: 8,
        brandId: 2,
        name: 'Fuego',
      },
      {
        id: 9,
        brandId: 1,
        name: 'Prest',
      },
      {
        id: 10,
        brandId: 1,
        name: 'Sonata',
      }
    ];

    let res: Array<any> = [];

    for (let i = 0; i < this.models.length; i++) {
      if (this.models[i].brandId === brandId) {
        res.push(this.models[i]);
      }
    }

    return res;
  }


  updateCar(carId, car) {
    for (let i = 0; i < this.cars.length; i++) {
      if (this.cars[i].id === carId) {
        this.cars[i] = {
          brand: car.brand,
          model: car.model,
          year: car.year,
          imgUrl: car.imgUrl
        };

        return this.cars;
      }
    }
  }

  addCar(car) {
    this.cars.push(car);

    return this.cars;
  }
}

