import { Injectable } from '@angular/core';
import {Router} from "@angular/router";

@Injectable()
export class CarFormService {
  isEdit: boolean;

  constructor(private router: Router) {
  }

  setFormStatus(status: boolean , id?: number){
    if (status) {
      this.isEdit = true;
      this.router.navigate(['cars/' + id + '']);
    }
    else {
      this.isEdit = false;
      this.router.navigate(['cars/add']);
    }
  }
}
