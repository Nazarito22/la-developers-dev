import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./auth/login/login.component";
import {CarEditComponent} from "./entities/cars/car-edit/car-edit.component";
import {CarListComponent} from "./entities/cars/car-list/car-list.component";
import {AuthGuard} from "./auth-guard.service";

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'cars', component: CarListComponent , canActivate: [AuthGuard] },
  { path: 'cars/:id', component: CarEditComponent , canActivate: [AuthGuard] },
  { path: 'cars/add', component: CarEditComponent , canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
