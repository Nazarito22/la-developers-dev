import { Component, OnInit } from '@angular/core';
import {CarsService} from "../../../api/cars.service";
import {Car} from "../../../models/car";
import {CarFormService} from "../../../shared/car-form.service";

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.scss']
})
export class CarListComponent implements OnInit {
  cars: Car[] = [];

  constructor(public carsService: CarsService,
              public carFormService: CarFormService) { }

  ngOnInit() {
    this.cars = this.carsService.getAll();
  }

  deleteCar(car){
    const index: number = this.cars.indexOf(car);
    if (index !== -1) {
      this.cars.splice(index, 1);
    }
  }
}
