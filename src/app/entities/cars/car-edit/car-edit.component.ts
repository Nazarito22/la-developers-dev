import {Component, OnInit} from '@angular/core';
import {CarsService} from "../../../api/cars.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Car} from "../../../models/car";
import {CarFormService} from "../../../shared/car-form.service";

@Component({
  selector: 'app-car-edit',
  templateUrl: './car-edit.component.html',
  styleUrls: ['./car-edit.component.scss']
})
export class CarEditComponent implements OnInit {

  brands: Array<any> = [];
  models: Array<any> = [];
  editCar: Car;
  brandId = 1;
  carId;
  isEdit: boolean;

  constructor(public carsService: CarsService,
              public carFormService: CarFormService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    if (!this.carFormService.isEdit) {
      this.route.params.subscribe(params => {
        console.log(params);
        this.carId = +params['id'];
        this.isEdit = true;
        this.editCar = this.carsService.getCar(this.carId);
      });
    }
    else {
      this.isEdit = false;
      this.editCar = null;
    }
    this.brands = this.carsService.getAllBrands();
    this.models = this.carsService.getModels(this.brandId);
  }

  updateCar(newCarInfo) {
    console.log(newCarInfo);
    this.carsService.updateCar(this.carId, newCarInfo);
    this.router.navigate(['/cars']);
  }

  addCar(newCarInfo) {
    console.log(newCarInfo);
    this.carsService.updateCar(this.carId, newCarInfo);
    this.router.navigate(['/cars']);
  }

  changeBrand(brandId) {
    console.log(brandId);
    this.models = this.carsService.getModels(brandId);
  }
}
