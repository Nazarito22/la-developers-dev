import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CarListComponent} from "./car-list/car-list.component";
import {CarEditComponent} from "./car-edit/car-edit.component";
import {
  MatButtonModule, MatCardModule, MatFormFieldModule, MatInputModule,
  MatSelectModule
} from "@angular/material";
import {CarsService} from "../../api/cars.service";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {CarFormService} from "../../shared/car-form.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    RouterModule
  ],
  declarations: [
    CarListComponent,
    CarEditComponent
  ],
  providers: [
    CarsService,
    CarFormService
  ]
})
export class CarsModule { }
