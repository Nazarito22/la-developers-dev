import {AuthHttp, AuthConfig} from 'angular2-jwt/angular2-jwt';
import {AuthGuard} from './auth-guard.service';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule, Http, BaseRequestOptions} from '@angular/http';
import {AppComponent} from './app.component';
import {AuthService} from "./api/auth.service";
import {AppRoutingModule} from "./app-routing.module";
import {MatButtonModule, MatCardModule, MatFormFieldModule, MatInputModule, MatSelectModule} from "@angular/material";
import {CarsModule} from "./entities/cars/cars.module";
import {LoginComponent} from "./auth/login/login.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MockBackend} from "@angular/http/testing";
import {fakeBackendProvider} from "./helpers/fake-backend";
import {OrderService} from "./api/order.service";
import {CarFormService} from "./shared/car-form.service";

export function getAuthHttp(http) {
  return new AuthHttp(new AuthConfig({
    tokenName: 'token'
  }), http);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    AppRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatCardModule,
    BrowserModule,
    BrowserAnimationsModule,
    CarsModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    OrderService,
    CarFormService,

    AuthService,
    AuthGuard,
    AuthHttp,
    {
      provide: AuthHttp,
      useFactory: getAuthHttp,
      deps: [Http]
    },

    // For creating a mock back-end. You don't need these in a real app.
    fakeBackendProvider,
    MockBackend,
    BaseRequestOptions
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
